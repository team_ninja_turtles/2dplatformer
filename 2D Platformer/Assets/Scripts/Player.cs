﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    //Floats - Variables
    public float maxSpeed = 3;
    public float speed = 50f;
    public float jumpPower = 200f;

    //Booleans
    public bool grounded;
    public bool doubleJump;

    //Stats
    public int currHealth;
    public int maxHealth = 5;

    //References
    private Rigidbody2D rb2d;
    private Animator anim;

	void Start () 
    {

        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();

        currHealth = maxHealth;

    }
	
	// Update is called once per frame
	void Update ()
    {

        anim.SetBool("Grounded", grounded);
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));

        //Flip Player
        if(Input.GetAxis("Horizontal") < -0.1f)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (grounded)
            {
                rb2d.AddForce(Vector2.up * jumpPower);
                doubleJump = true;
            }

            //Checks if player can double jump
            if(doubleJump && !grounded)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
                rb2d.AddForce(Vector2.up * (jumpPower * 0.75f));
                doubleJump = false;
            }
        }

        
        if (currHealth > maxHealth)
        {
            currHealth = maxHealth;
        }

        if (currHealth <= 0)
        {
            Die();
        }

    }

    void FixedUpdate()
    {

        Vector3 easeVelocity = rb2d.velocity;
        easeVelocity.y = rb2d.velocity.y;
        easeVelocity.x *= 0.75f;
        easeVelocity.z = 0.0f;

        float h = Input.GetAxis("Horizontal");

        if (grounded)
        {
            rb2d.velocity = easeVelocity;
        }


        //Moving the player
        rb2d.AddForce((Vector2.right * speed) * h);


        //Limit the speed of the player
        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }

        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);

        }

    }

    void Die()
    {
        //Reloads the Scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Damage(int dmg)
    {
        if(currHealth < dmg)
        {
            dmg = currHealth;
        }

        currHealth -= dmg;
    }
}
